package shop.dev.choibk.api.kakao;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import shop.dev.choibk.api.kakao.enums.KakaoUri;
import shop.dev.choibk.api.kakao.model.ConvertAddressResponse;
import shop.dev.choibk.api.kakao.model.SearchAddressResponse;
import shop.dev.choibk.api.kakao.model.SearchRegionResponse;

import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@SpringBootTest
public class ApiKakaoApplicationTests {

    @Test
    void contextLoads() {

    }

    @Test
    void apiCallTest() {
        String apiDomain = "https://dapi.kakao.com";
        String apiFullUri = apiDomain + KakaoUri.SEARCH_ADDRESS.getApiSubUri();

        String addressValue = "고잔로 88";
        String queryString = "?query=" + URLEncoder.encode(addressValue, StandardCharsets.UTF_8);

        URI uri = URI.create(apiFullUri + queryString);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();

        headers.add("Authorization", "KakaoAK 8437a8e20194cd38145d1c22242fdd34");
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE + ";charset=UTF-8");

        RequestEntity<String> requestEntity = new RequestEntity<>(headers, HttpMethod.GET, uri);
        ResponseEntity<SearchAddressResponse> responseEntity = restTemplate.exchange(requestEntity, SearchAddressResponse.class);

        SearchAddressResponse result = responseEntity.getBody();

        int test = 1;
    }


    @Test
    void apiCallTest2() {
        String apiDomain = "https://dapi.kakao.com";
        String apiFullUri = apiDomain + KakaoUri.SEARCH_REGION.getApiSubUri();

        String x = "127.1086228";
        String y = "37.4012191";
        String queryString = "?x=" + x + "&y=" + y;

        URI uri = URI.create(apiFullUri + queryString);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();

        headers.add("Authorization", "KakaoAK 8437a8e20194cd38145d1c22242fdd34");
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE + ";charset=UTF-8");

        RequestEntity<String> requestEntity = new RequestEntity<>(headers, HttpMethod.GET, uri);

        ResponseEntity<SearchRegionResponse> responseEntity = restTemplate.exchange(requestEntity, SearchRegionResponse.class);
        SearchRegionResponse result = responseEntity.getBody();

        int test = 2;
    }
    @Test
    void apiCallTest3() {
        String apiDomain = "https://dapi.kakao.com";
        String apiFullUri = apiDomain + KakaoUri.CONVERT_ADDRESS.getApiSubUri();

        String x = "127.1086228";
        String y = "37.4012191";
        String coord = "WGS84";
        String queryString = "?x=" + x + "&y=" + y + "&input_coord=" + coord;

        URI uri = URI.create(apiFullUri + queryString);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();

        headers.add("Authorization", "KakaoAK 8437a8e20194cd38145d1c22242fdd34");
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE + ";charset=UTF-8");

        RequestEntity<String> requestEntity = new RequestEntity<>(headers, HttpMethod.GET, uri);
        ResponseEntity<ConvertAddressResponse> responseEntity = restTemplate.exchange(requestEntity, ConvertAddressResponse.class);

        ConvertAddressResponse result = responseEntity.getBody();

        int test = 1;
    }
}
