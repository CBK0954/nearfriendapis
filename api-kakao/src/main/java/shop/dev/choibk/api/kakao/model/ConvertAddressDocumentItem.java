package shop.dev.choibk.api.kakao.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConvertAddressDocumentItem {
    private ConvertAddressDocumentInRoadAddressResponse road_address;
    private ConvertAddressDocumentInAddressResponse address;
}
