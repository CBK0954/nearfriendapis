package shop.dev.choibk.api.kakao.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchRegionDocumentItem {
    @ApiModelProperty(notes = "H(행정동) 또는 B(법정동)")
    private String region_type;

    @ApiModelProperty(notes = "지역 1Depth, 시도 단위")
    private String region_1depth_name;

    @ApiModelProperty(notes = "지역 2Depth, 구 단위")
    private String region_2depth_name;

    @ApiModelProperty(notes = "지역 3Depth, 동 단위")
    private String region_3depth_name;

    @ApiModelProperty(notes = "지역 4Depth, region_type이 법정동이며, 리 영역인 경우만 존재")
    private String region_4depth_name;
}
