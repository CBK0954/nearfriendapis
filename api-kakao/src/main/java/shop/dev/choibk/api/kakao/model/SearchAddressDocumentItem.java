package shop.dev.choibk.api.kakao.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchAddressDocumentItem {
    @ApiModelProperty(notes = "전체 지번 주소 또는 전체 도로명 주소, 입력에 따라 결정됨")
    private String address_name;

    @ApiModelProperty(notes = "address_name의 값의 타입(Type)\n" +
            "다음 중 하나:\n" +
            "REGION(지명)\n" +
            "ROAD(도로명)\n" +
            "REGION_ADDR(지번 주소)\n" +
            "ROAD_ADDR(도로명 주소)")
    private String address_type;

    @ApiModelProperty(notes = "X 좌표값, 경위도인 경우 경도(longitude)")
    private String x;

    @ApiModelProperty(notes = "Y 좌표값, 경위도인 경우 위도(latitude)")
    private String y;
}
