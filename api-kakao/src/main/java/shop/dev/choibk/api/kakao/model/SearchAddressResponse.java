package shop.dev.choibk.api.kakao.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SearchAddressResponse {
    @ApiModelProperty(notes = "결과 아이템들")
    private List<SearchAddressDocumentItem> documents;
}