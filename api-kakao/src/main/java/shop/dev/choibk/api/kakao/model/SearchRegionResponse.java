package shop.dev.choibk.api.kakao.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SearchRegionResponse {
    private List<SearchRegionDocumentItem> documents;
}