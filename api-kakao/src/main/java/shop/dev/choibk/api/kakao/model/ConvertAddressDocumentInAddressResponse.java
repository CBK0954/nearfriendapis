package shop.dev.choibk.api.kakao.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConvertAddressDocumentInAddressResponse {
    private String address_name;
}
