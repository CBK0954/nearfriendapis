package shop.dev.choibk.api.kakao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiKakaoApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiKakaoApplication.class, args);
    }
}