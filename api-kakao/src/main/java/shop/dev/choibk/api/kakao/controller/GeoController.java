package shop.dev.choibk.api.kakao.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import shop.dev.choibk.api.kakao.model.ConvertAddressResponse;
import shop.dev.choibk.api.kakao.model.SearchAddressResponse;
import shop.dev.choibk.api.kakao.model.SearchRegionResponse;
import shop.dev.choibk.api.kakao.service.GeoService;
import shop.dev.choibk.common.response.model.SingleResult;
import shop.dev.choibk.common.response.service.ResponseService;

@Api(tags = "좌표 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/geo")
public class GeoController {
    private final GeoService geoService;

    @ApiOperation(value = "좌표를 주소로 변환하기 (좌표를 지번주소, 도로명주소로 변환)")
    @GetMapping("/convert/address")
    public SingleResult<ConvertAddressResponse> getConvertAddress(@RequestParam("x") String x, @RequestParam("y") String y) {
        return ResponseService.getSingleResult(geoService.getConvertAddress(x, y));
    }

    @ApiOperation(value = "주소를 좌표로 변환하기 (주소를 좌표로 변환)")
    @GetMapping("/search/address")
    public SingleResult<SearchAddressResponse> getSearchAddress(@RequestParam("searchAddress") String searchAddress) {
        return ResponseService.getSingleResult(geoService.getSearchAddress(searchAddress));
    }

    @ApiOperation(value = "행정구역정보 검색하기 (좌표를 지번주소, 행정구역상주소로 변환)")
    @GetMapping("/search/region")
    public SingleResult<SearchRegionResponse> getSearchRegion(@RequestParam("x") String x, @RequestParam("y") String y) {
        return ResponseService.getSingleResult(geoService.getSearchRegion(x, y));
    }
}