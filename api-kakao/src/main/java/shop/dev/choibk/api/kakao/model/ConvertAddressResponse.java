package shop.dev.choibk.api.kakao.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ConvertAddressResponse {
    private List<ConvertAddressDocumentItem> documents;
}
