package shop.dev.choibk.api.kakao.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum KakaoUri {
    SEARCH_ADDRESS("주소 검색하기", "/v2/local/search/address.json"),
    SEARCH_REGION("행정구역정보 검색하기", "/v2/local/geo/coord2regioncode.json"),
    CONVERT_ADDRESS("주소 변환하기", "/v2/local/geo/coord2address.json");

    private final String apiName;
    private final String apiSubUri;
}