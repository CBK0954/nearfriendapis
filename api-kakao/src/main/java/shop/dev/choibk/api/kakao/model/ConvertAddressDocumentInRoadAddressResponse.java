package shop.dev.choibk.api.kakao.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConvertAddressDocumentInRoadAddressResponse {
    private String address_name;

    private String building_name;

    private String zone_no;
}
