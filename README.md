# 동네 친구 찾기 API
- 카카오 API 를 이용하여 posX, posY (위도, 경도)를 기반으로 주변 구역을 찾고 지번 주소와 도로명 주소로 반환하는 API 입니다.

### 사용한 언어
```
- java 17
- JPA
- Postgres(+ Procedure)
```

### 기술
```
1. Kakao API를 이용한 주소 <-> 좌표 변환
2. 좌표를 이용하여 건물명, 지번주소, 도로명주소 검색
3. 좌표를 이용하여 행정구역상 주소 검색
3. Kakao API를 이용하여 근처나 먼 구역을 등록
4. Kakao API를 이용하여 등록된 구역을 입력된 반경(m) 내에 검색
```

### 필요 프로시저
```
CREATE OR REPLACE FUNCTION public.get_near_friends(positionX DOUBLE PRECISION, positionY DOUBLE PRECISION, distance DOUBLE PRECISION)
RETURNS TABLE
(
    nickname varchar
    , hobby varchar
    , gender varchar
    , distance_m double precision
)
as
$$
DECLARE
    v_record RECORD;
BEGIN
    for v_record in (
        select
            member.nickname, member.hobby, member.gender, earth_distance(ll_to_earth(member.posy, member.posx), ll_to_earth(positionY, positionX)) as distance_m
        from member
        where earth_distance(ll_to_earth(member.posy, member.posx), ll_to_earth(positionY, positionX)) <= distance
        order by distance_m asc
    )
    loop
        nickname := v_record.nickname;
        hobby := v_record.hobby;
        gender := v_record.gender;
        distance_m := v_record.distance_m;
        return next;
    end loop;
END;
$$
LANGUAGE plpgsql
```

##### 제작기간
```
- 2023.03.06 ~ 2023.03.10
```