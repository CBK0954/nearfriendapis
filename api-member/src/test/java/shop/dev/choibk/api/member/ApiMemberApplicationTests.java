package shop.dev.choibk.api.member;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import shop.dev.choibk.api.member.entity.Members;
import shop.dev.choibk.api.member.enums.Gender;
import shop.dev.choibk.api.member.repository.MembersRepository;

import java.util.List;

@SpringBootApplication
public class ApiMemberApplicationTests {
    @Autowired
    MembersRepository membersRepository;
    @Test
    void contextLoads() {

    }

    @Test
    void dataTest1() {
        double searchX = 126.834249957013;
        double searchY = 37.3189531318832;
        double searchDistance = 10000;

        List<Members> result = membersRepository.findAllByNearFriends(searchY, searchX, searchDistance);

        int test = 1;
    }
    @Test
    void dataTest2() {
        double searchX = 126.835639929462;
        double searchY = 37.3179417691029;
        double searchDistance = 200;
        String searchGender = Gender.WOMAN.toString();

        List<Members> result = membersRepository.findAllByNearFriendsByGender(searchY, searchX, searchDistance, searchGender);

        int test = 1;
    }

}