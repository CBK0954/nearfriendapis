package shop.dev.choibk.api.member.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class NearFriendSearchRequest {
    @ApiModelProperty(notes = "내 위치 x 좌표")
    @NotNull
    private Double posX;

    @ApiModelProperty(notes = "내 위치 y 좌표")
    @NotNull
    private Double posY;

    @ApiModelProperty(notes = "최대 거리 설정(반경)")
    @NotNull
    private Integer distance;
}
