package shop.dev.choibk.api.member.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.dev.choibk.api.member.entity.Members;
import shop.dev.choibk.api.member.model.MembersCreateRequest;
import shop.dev.choibk.api.member.model.NearFriendItem;
import shop.dev.choibk.api.member.repository.MembersRepository;
import shop.dev.choibk.common.response.model.ListResult;
import shop.dev.choibk.common.response.service.ListConvertService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MembersService {
    private final MembersRepository membersRepository;

    @PersistenceContext
    EntityManager entityManager;
    public void setMembers(MembersCreateRequest createRequest) {
        Members members = new Members.Builder(createRequest).build();
        membersRepository.save(members);
    }
    /**
     * 근처 친구 리스트를 가져옴
     * @param posX ex) 126.xxx
     * @param posY ex) 37.xxx
     * @param distance 몇 km의 친구 리스트를 가져올지 결정 ex) 1,2,...
     * @return
     * */
    public ListResult<NearFriendItem> getNearFriends(double posX, double posY, int distance) {
        double distanceResult = distance * 1; // 미터로 변환 ( * 1 = 입력 값[숫자] * 1m)

        String queryString = "select * from public.get_near_friends(" + posX + ", " + posY + ", " + distanceResult + ")";
        Query nativeQuery = entityManager.createNativeQuery(queryString);
        List<Object[]> resultList = nativeQuery.getResultList(); // 실행해서 가져오기

        List<NearFriendItem> result = new LinkedList<>();
        for(Object[] resultItem : resultList) {
            result.add(
                    new NearFriendItem.Builder(
                            resultItem[0].toString(),
                            resultItem[1].toString(),
                            resultItem[2].toString(),
                            Double.parseDouble(resultItem[3].toString())).build()
            );
        }
        return ListConvertService.settingResult(result);
    }
}