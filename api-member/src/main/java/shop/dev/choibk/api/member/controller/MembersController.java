package shop.dev.choibk.api.member.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import shop.dev.choibk.api.member.model.MembersCreateRequest;
import shop.dev.choibk.api.member.model.NearFriendItem;
import shop.dev.choibk.api.member.model.NearFriendSearchRequest;
import shop.dev.choibk.api.member.service.MembersService;
import shop.dev.choibk.common.response.model.CommonResult;
import shop.dev.choibk.common.response.model.ListResult;
import shop.dev.choibk.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "주변 친구 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MembersController {
    private final MembersService membersService;

    @ApiOperation(value = "주변 친구 등록")
    @PostMapping("/friend")
    public CommonResult setMembers(@RequestBody @Valid MembersCreateRequest createRequest) {
        membersService.setMembers(createRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "내 주변 친구 찾기")
    @PostMapping("/search/friends") // 기능은 R(Get)이지만 body 를 쓰기 위해 PostMapping 사용
    public ListResult<NearFriendItem> getFriends(@RequestBody @Valid NearFriendSearchRequest searchRequest) {
        return ResponseService.getListResult(membersService.getNearFriends
                (searchRequest.getPosX(), searchRequest.getPosY(), searchRequest.getDistance()), true);
    }
}
