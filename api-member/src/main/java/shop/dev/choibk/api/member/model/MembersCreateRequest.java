package shop.dev.choibk.api.member.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import shop.dev.choibk.api.member.enums.Gender;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MembersCreateRequest {
    @ApiModelProperty(notes = "별명")
    @NotNull
    @Length(min = 2, max = 20)
    private String nickname;

    @ApiModelProperty(notes = "취미")
    @NotNull
    @Length(min = 2, max = 50)
    private String hobby;

    @ApiModelProperty(notes = "성별")
    @NotNull
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @ApiModelProperty(notes = "x 좌표")
    @NotNull
    private Double posX;

    @ApiModelProperty(notes = "y 좌표")
    @NotNull
    private Double posY;
}

