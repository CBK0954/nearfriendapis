package shop.dev.choibk.api.member.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import shop.dev.choibk.api.member.entity.Members;

import java.util.List;

public interface MembersRepository extends JpaRepository<Members, Long> {
    @Query(
            value = "select * from members where earth_distance(ll_to_earth(posy, posx), ll_to_earth(:pointY, :pointX)) <= :searchDistance",
            nativeQuery = true
    )
    List<Members> findAllByNearFriends(@Param("pointY") double pointY, @Param("pointX") double pointX, @Param("searchDistance") double searchDistance);
    @Query(
            value = "select * from member where earth_distance(ll_to_earth(posy, posx), ll_to_earth(:pointY, :pointX)) <= :searchDistance and member.gender = :gender",
            nativeQuery = true
    )
    List<Members> findAllByNearFriendsByGender(@Param("pointY") double pointY, @Param("pointX") double pointX, @Param("searchDistance") double searchDistance, @Param("gender") String gender);

}