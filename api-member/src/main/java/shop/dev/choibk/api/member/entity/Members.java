package shop.dev.choibk.api.member.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.dev.choibk.api.member.enums.Gender;
import shop.dev.choibk.api.member.model.MembersCreateRequest;
import shop.dev.choibk.common.interfaces.CommonModelBuilder;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Members {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "별명")
    @Column(nullable = false, length = 20)
    private String nickname;

    @ApiModelProperty(notes = "취미")
    @Column(nullable = false, length = 50)
    private String hobby;

    @ApiModelProperty(notes = "성별")
    @Column(nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @ApiModelProperty(notes = "x 좌표")
    @Column(nullable = false)
    private Double posX;

    @ApiModelProperty(notes = "y 좌표")
    @Column(nullable = false)
    private Double posY;

    private Members(Builder builder) {
        this.nickname = builder.nickname;
        this.hobby = builder.hobby;
        this.gender = builder.gender;
        this.posX = builder.posX;
        this.posY = builder.posY;
    }

    public static class Builder implements CommonModelBuilder<Members> {
        private final String nickname;
        private final String hobby;
        private final Gender gender;
        private final Double posX;
        private final Double posY;

        public Builder(MembersCreateRequest createRequest) {
            this.nickname = createRequest.getNickname();
            this.hobby = createRequest.getHobby();
            this.gender = createRequest.getGender();
            this.posX = createRequest.getPosX();
            this.posY = createRequest.getPosY();
        }

        @Override
        public Members build() {
            return new Members(this);
        }
    }
}

